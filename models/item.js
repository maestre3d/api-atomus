'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemSchema = Schema({
    practice: {type: Schema.ObjectId, ref: 'Practice'},
    material: {type: Schema.ObjectId, ref: 'Material'},
    quantity: Number
});

module.exports = mongoose.model('Item', ItemSchema);