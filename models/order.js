'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = Schema({
    iat: Number,
    exp: Number,
    status: String,
    user: {type: Schema.ObjectId, ref: 'User'},
    practice: {type: Schema.ObjectId, ref: 'Practice'},
    items: [{type: Schema.ObjectId, ref: 'Item'}]
});

module.exports = mongoose.model('Order', OrderSchema);