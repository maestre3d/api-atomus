'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    username:       String,
    email:          String,
    name:           String,
    surname:        String,
    grade:          Number,
    password:       String,
    image:          String,
    deviceId:       String,
    course:         [{ type : Schema.ObjectId, ref : 'Course'}]
});

module.exports = mongoose.model('User', UserSchema);