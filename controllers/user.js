/*
    *   AUTHOR: ALONSO R
    *   DATE: 2/18/2019
    *   DESC: Class to manage users/students.
    *   LICENSE: CLOSED - SOURCE
*/

'use strict'
// Imports
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var jwt = require('../services/jwt');
var fs = require('fs');
var path = require('path');
var handlebars = require('handlebars');
var moment = require('moment');
var admin = require('firebase-admin');

// Model(s)
var User = require('../models/user');
var Ban = require('../models/ban');
var Course = require('../models/course');
var Practice = require('../models/practice');

// Misc
const apiMsg = 'Server Error.';
const saltRounds = 10;
const transporter = nodemailer.createTransport({
    service: 'gmail',
    secure: true,
    auth: {
        user: 'luis.mccartney.16@gmail.com',
        pass: 'BUNGIE100'
    }
});
const htPath = './misc/mail.html';


// Creates new user/student
function newUser(req, res){
    var user = new User();
    var params = req.body;

    user.username = params.username;
    user.name = params.name;
    user.surname = params.surname;
    user.grade = params.grade;
    user.email = params.email;
    user.deviceId = null;
    user.image = null;

    if(params.username && params.password && params.email){

        bcrypt.hash(params.password, saltRounds, function(err, hash){
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                user.password = hash;
                if(user.name && user.surname && user.grade){
                    User.findOne({$or: [{username:user.username}, {email: user.email}]}, (err, found)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(!found){
                                user.save((err, newUser)=>{
                                    if(err){
                                        res.status(500).send({message:apiMsg});
                                    }else{
                                        if(!newUser){
                                            res.status(404).send({message:"Error al crear usuario."});
                                        }else{
                                            readHTMLFile(htPath, function(err, html) {
                                                var template = handlebars.compile(html);
                                                var replacements = {
                                                     username: user.name
                                                };
                                                var htmlToSend = template(replacements);

                                                var mailOptions = {
                                                    from: 'luis.mccartney.16@gmail.com',
                                                    to: user.email,
                                                    subject: 'Confirmación de inscripción a la App de la Facultad De Ciencias Químicas UACH',
                                                    html : htmlToSend
                                                 };

                                                 transporter.sendMail(mailOptions, function(err, info){
                                                    if(err){
                                                        res.status(500).send({message:err});
                                                    }else{
                                                        console.log('Email sent: ' + info.response);
                                                        res.status(200).send(newUser);
                                                    }
                                                });
                                            });
                                            

                                        }
                                    }
                                });
                            }else{
                                res.status(406).send({message:"Usuario ya ha sido creado."});
                            }
                        }
                    });
                }else{
                    res.status(400).send({message:"Inserte todos los campos."});
                }
            }
        });
    }else{
        res.status(400).send({message:"Inserte todos los campos."});
    }
}

// Student's login
function logUser(req, res){
    var params = req.body;
    var hash;

    var username = params.username;
    var password = params.password;

    if(params.username && params.password){
        User.findOne({username:username}, (err, user)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(user){
                    hash = user.password;
                    bcrypt.compare(password, hash, function(err, logged){
                        if(logged === true){
                            // Logs correctly
                            Ban.findOne({user: user._id}, (err, banUser)=>{
                                if(err){
                                    res.status(500).send({message:apiMsg});
                                }else{
                                    if(banUser){
                                        res.status(403).send({message:"Has sido vetado de la plataforma temporalmente.\n\nContacta con administración académica."});
                                    }else{
                                        setDevice
                                        if(params.gethash){
                                            res.status(200).send({token:jwt.createToken(user)});
                                        }else{
                                            res.status(200).send(user);
                                        }
                                    }
                                }
                            });
                        }else{
                            res.status(404).send({message:"Usuario y/o contraseña incorrectos."});
                        }
                    });
                }else{
                    res.status(404).send({message:"Usuario y/o contraseña incorrectos."});
                }
            }
        });
    }else{
        res.status(400).send({message:"Inserte todos los campos."});
    }
}

// Updates student
function updateUser(req, res){
    var userId = req.params.id;
    var user = req.body;

    if(userId != req.user._id){
        return res.status(401).send({message:"Acceso denegado."});
    }

    if(user.username && !req.user.role){
        return res.status(403).send({message:"Acción inválida."});
    }

    User.findOne({$or:  [{username:user.username}, {email: user.email}]}, (err, found)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!found){
                if(user.password){
                    bcrypt.hash(user.password, saltRounds, function(err, hash){
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            user.password = hash;
                            User.findByIdAndUpdate(userId, user, (err, updated)=>{
                                if(err){
                                    res.status(500).send({message:apiMsg});
                                }else{
                                    if(!updated){
                                        res.status(404).send({message:"Error al actualizar usuario."});
                                    }else{
                                        res.status(200).send(updated);
                                    }
                                }
                            });
                        }
                    });
                }else{
                    User.findByIdAndUpdate(userId, user, (err, updated)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(!updated){
                                res.status(404).send({message:"Error al actualizar usuario."});
                            }else{
                                res.status(200).send(updated);
                            }
                        }
                    });
                }
            }else{
                res.status(400).send({message:"Usuario ya creado."});
            }
        }
    });
}

// Deletes student
function deleteUser(req, res){
    var userId = req.params.id;
    var path_file = './uploads/users/';

    if(!req.user.role){
        return res.status(403).send({message:"Acceso denegado."});
    }

    User.findByIdAndRemove(userId, (err, userRm)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!userRm){
                res.status(404).send({message:"Error al borrar usuario."});
            }else{
                // DELETE ON CASCADE
                // User=>Order=>Ban=>Item
                Order.find({user:userRm._id}).deleteMany((err, orderRm)=>{
                    if(err){
                        res.status(500).send({message:apiMsg}); 
                    }else{
                        if(!orderRm){
                            res.status(404).send({message:"Error al eliminar."});
                        }else{
                            Ban.find({order: orderRm._id}).deleteOne((err, banRm)=>{
                                if(err){
                                    res.status(500).send({message:apiMsg});
                                }else{
                                    if(!banRm){
                                        res.status(404).send({message:"Error al eliminar."});
                                    }else{
                                        Item.find({practice: orderRm.practice._id}).deleteMany((err, itRm)=>{
                                            if(err){
                                                res.status(500).send({message:apiMsg});
                                            }else{
                                                if(!itRm){
                                                    res.status(404).send({message:"Error al eliminar."});
                                                }else{
                                                    path_file = path_file + userRm.image;
                                                    fs.unlink(path_file, (err)=>{
                                                        if(err){
                                                            res.status(500).send({message:"Error al borrar archivo."});
                                                        }
                                                        //console.log('Successfully deleted file.');
                                                    });
                                                    res.status(200).send(userRm);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

// Get all students
function getUsers(req, res){
    User.find((err, users)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!users){
                res.status(404).send({message:"No se encontraron usuarios."});
            }else{
                res.status(200).send(users);
            }
        }
    });
}

// Get student populating student's courses
function getUser(req, res){
    var userId = req.params.id;
    var find = User.findById(userId);

    find
    .populate({
        path:'course',
        populate: {path:'teacher lab'}
    })
    .exec((err, found)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!found){
                res.status(404).send({message:"No se encontró usuario."});
            }else{
                res.status(200).send(found);
            }
        }
    });
}

// Attach new course to student
function addCourse(req, res){
    var userId = req.params.id;
    var user = req.body;

    // These registration tokens come from the client FCM SDKs.
    var registrationTokens = [
        req.user.deviceId
    ];
  

    if(user.course){
        User.findOne( {$and: [{course: user.course}, { _id: userId }]} , (err, found)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(found){
                    res.status(400).send({message:"Usuario ya está inscrito al curso."});
                }else{
                    Course.findById(user.course, (err, course)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(!course){
                                res.status(404).send({message:"Curso no encontrado."});
                            }else{
                                User.findByIdAndUpdate(userId, 
                                    ({'course': { '$ne': user.course } }, 
                                    {'$addToSet': { 'course': user.course } }), 
                                    (err, saved)=>{
                                        if(err){
                                            res.status(500).send({message:apiMsg});
                                        }else{
                                            if(!saved){
                                                res.status(404).send({message:"Error al añadir curso."});
                                            }else{
                                                if(saved.deviceId){
                                                    var topic = course.topic;
                                                    // Subscribe the devices corresponding to the registration tokens to the
                                                    // topic.
                                                    admin.messaging().subscribeToTopic(registrationTokens, topic)
                                                    .then(function(response) {
                                                    // See the MessagingTopicManagementResponse reference documentation
                                                    // for the contents of response.
                                                    console.log('Successfully subscribed to topic:', response);
                                                    })
                                                    .catch(function(error) {
                                                    console.log('Error subscribing to topic:', error);
                                                    });
                                                }
            
                                                res.status(200).send(saved);
                                            }
                                        }
                                });
                            }
                        }
                    });
                }
            }
        });
    }else{
        res.status(404).send({message:"Valor inválido."});
    }
}

// Detach course from student
function removeCourse(req, res){
    var userId = req.params.id;
    var user = req.body;

    // These registration tokens come from the client FCM SDKs.
    var registrationTokens = [
        req.user.deviceId
    ];
      

    if(user.course){
        User.findOne( {$and: [{course: user.course}, {_id: userId}]}, (err, found)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!found){
                    res.status(404).send({message:"No se encontró curso."});
                }else{
                    User.findByIdAndUpdate(userId, {$pull:{ 'course': user.course }}, (err, upUser)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(!upUser){
                                res.status(404).send({message:"Error al borrar curso."});
                            }else{
                                var topic = user.course.topic;
                                // Subscribe the devices corresponding to the registration tokens to the
                                // topic.
                                admin.messaging().unsubscribeFromTopic(registrationTokens, topic)
                                .then(function(response) {
                                // See the MessagingTopicManagementResponse reference documentation
                                // for the contents of response.
                                console.log('Successfully unsubscribed to topic:', response);
                                })
                                .catch(function(error) {
                                console.log('Error unsubscribing to topic:', error);
                                });
                                res.status(200).send(upUser);
                            }
                        }
                    } );
                }
            }
        });
    }else{
        res.status(400).send({message:"Valor inválido."});
    }
}

// FS: Uploads user's pic
function uploadImage(req, res){
    var userId = req.params.id;
    var file_name = 'Sin subir.';

    if(req.files){
        var path_file = './uploads/users/';
        var file_path = req.files.image.path;
        let file_name = path.basename(file_path);

        let ext_split = file_name.split('\.');
        let file_ext = ext_split[1];

        User.findById(userId, (err, found)=>{
            if(err){
                path_file = path_file + file_name;
                fs.unlink(path_file, (err)=>{
                    if(err){
                    }
                });
                res.status(500).send({message:apiMsg});
            }else{
                if(!found){
                    path_file = path_file + file_name;
                    fs.unlink(path_file, (err)=>{
                        if(err){
                        }
                    });
                    res.status(404).send({message:"Usuario no encontrado."});
                }else{
                    path_file = path_file + found.image;
                    if( file_ext === 'png' || file_ext === 'jpg' || file_ext === 'jpeg' ){
                        User.findByIdAndUpdate(userId, {image:file_name},(err, updated)=>{
                            if(err){
                                res.status(500).send({message:apiMsg});
                            }else{
                                
                                if(!updated){
                                    res.status(404).send({message:"Error al subir archivo."});
                                }else{
                                    if(found.image === null){
                                        res.status(200).send(updated);
                                    }else{
                                        fs.unlink(path_file, (err)=>{
                                            if(err){
                                                res.status(500).send({message:"Error al subir archivo."});
                                            }
                                            //console.log('Successfully deleted file.');
                                        });
                                        res.status(200).send(updated);
                                    }
                                }
                            }
                        });
                    }else{
                        res.status(406).send({message:"Extensión de archivo no soportada."});
                    }
                }
            }
        });
    }else{
        res.status(400).send({message:"Archivo no subido."});
    }
}

// Get user's pic
function getImageFile(req, res){
    var imageFile = req.params.imageFile;

    var path_file = './uploads/users/' + imageFile;

    fs.exists(path_file, function(exists){
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(404).send({message:"Archivo no encontrado."});
        }
    });
}


// Read email html template
var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            res.status(500).send({message:apiMsg});
        }
        else {
            callback(null, html);
        }
    });
};

function homeFeed(req, res){
    var userId = req.user._id;

    User.findById(userId, (err, user)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(user){
                const x = user.course;

                var find = Practice.find({ $and: [ {expDate: { $gt: moment().unix()}}, {course: x} ] })
                .sort({expDate: 'asc'});
        
                find.populate({
                    path:'course items',
                    populate: {
                        path: 'lab teacher material'
                    }
                })
                .exec((err, practices)=>{
                    if(err){
                        res.status(500).send({message:apiMsg});
                    }else{
                        if(practices){
                            if(practices.length>0){
                                res.status(200).send(practices);
                            }else{
                                res.status(404).send({message:"No hay prácticas a realizar."});
                            }
                        }
                    }
                });
            }else{
                res.status(404).send({message:"Usuario no encontrado."});
            }
        }
    });
}

function setDevice(req, res){
    var params = req.body;
    var device = params.deviceId;

    if(req.user._id && device){
        User.findByIdAndUpdate(req.user._id, {deviceId: device}, (err, user)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!user){
                    res.status(404).send({message:"Error al asignar dispositivo."});
                }else{
                    res.status(200).send({message:"El dispositivo ha sido registrado a " + user._id});
                }
            }
        });
    }else{
        res.status(404).send({message:"Usuario no encontrado."});
    }
}

function sendNotification(req, res){
    var userId = req.params.id;
    var params = req.body;

    var title = params.title;
    var body = params.body;

    if(params.title && params.body){

        User.findById(userId, (err, user)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!user){
                    res.status(404).send({message:"Usuario no encontrado."});
                }else{
                    // See documentation on defining a message payload.
                    var message = {
                        token:user.deviceId,
                        android: {
                            ttl: 86400 * 1000, // 1 hour in milliseconds
                            priority: 'high',
                            notification:{
                                title:title,
                                body:body
                            }
                        },
                        data: {
                            user: userId
                        }
                    };

                    // Send a message to the device corresponding to the provided
                    // registration token.
                    admin.messaging().send(message)
                    .then((response) => {
                        // Response is a message ID string.
                        console.log('Successfully sent message:', response);
                        res.status(200).send(user);
                    })
                    .catch((error) => {
                        console.log('Error sending message:', error);
                        res.status(500).send({message:"Error al mandar notificacion."});
                    });
                }
            }
        });
    }else{
        res.status(404).send({message:"Ingresa todos los campos."});
    }
}

function unsetDevice( req, res ){
    if(req.user._id && req.user.deviceId != null){
        User.find(({$and: [{_id: req.user._id}, {deviceId: req.user.deviceId}]})).updateOne({deviceId: null},(err, user)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!user){
                    res.status(404).send({message:"Usuario/Dispositivo no encontrado."});
                }else{
                    res.status(200).send({message:"El dispositivo ha sido removido de " + user._id});
                }
            }
        });
    }else{
        res.status(404).send({message:"Usuario no válido."});
    }
}

module.exports = {
    newUser,
    logUser,
    updateUser,
    deleteUser,
    getUsers,
    getUser,
    addCourse,
    removeCourse,
    uploadImage,
    getImageFile,
    homeFeed,
    setDevice,
    sendNotification,
    unsetDevice
};