/*
    *   AUTHOR: ALONSO R
    *   DATE: 2/19/2019
    *   DESC: Class to manage orders.
    *   LICENSE: CLOSED - SOURCE
*/

'use strict'
// Imports
var moment = require('moment');
var admin = require("firebase-admin");

// Model(s)
var Order = require('../models/order');
var Practice = require('../models/practice');
var User = require('../models/user');
var Material = require('../models/material');
var Ban  = require('../models/ban');
var Item = require('../models/item');

// Misc
const apiMsg = 'Server Error.';

// Test function for UNIX's Time Stamp & momentjs 
function testExp(req, res){
    var date = moment().unix();
    //var tmp = moment.unix(date).format('dddd, MMMM Do, YYYY h:mm:ss A');
    var tmp = moment.unix(date); 
    var exp = moment(tmp).add(2, 'days');
    console.log(moment().unix());
    res.status(200).send({time: moment(exp).add((10), 'days').toLocaleString()});
};

function testUpdate(req, res){
    var orderId = req.params.id;
    Order.findById(orderId).then(function(order) {
        var matQ = [];
      
        order.forEach(function(u) {
          matQ.push(Material.find({material:u.material}));
        });
      
        return Promise.all(matQ );
      }).then(function(list) {
          res.status(200).send(list);
      }).catch(function(error) {
          res.status(500).send('one of the queries failed', error);
      }
    ); 
}

// Creates new order
// requires: user_id, practice_id
// note: (It fills automatically order's mat list with practice's mat list).
function createOrder(req, res){
    var order = new Order();
    var params = req.body;


    order.iat = moment().unix();
    order.user= req.user._id;
    order.status = "PENDIENTE";
    order.practice = params.practice;


    if(order.user && order.practice){

        Practice.findById(order.practice, (err, practice)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!practice){
                    res.status(404).send({message:"Práctica inexistente."});
                }else{
                    Order.findOne({$and: [{user: order.user}, {practice: order.practice}]}, (err, found)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(found){
                                res.status(404).send({message:"Ya has ordenado para esta práctica."});
                            }else{
                                var tmp = moment.unix(practice.expDate); 
                                order.material = practice.items;
                                order.exp = moment(tmp).add(5, 'days').unix();



                                User.findOne({$and: [{_id:order.user}, {course: practice.course}]}, (err, user)=>{
                                    if(err){
                                        res.status(500).send({message:apiMsg});
                                    }else{
                                        if(!user){
                                            res.status(404).send({message:"Usuario inexistente."});
                                        }else{
                                            var x = practice.items;
            
                                            Item.find({_id: x},(err, items)=>{
                                                if(err){
                                                    res.status(500).send({message:apiMsg});
                                                }else{
                                                    if(!items || items.length === 0){
                                                        res.status(404).send({message:"Esta práctica no contiene material."});
                                                    }else{
                                                        var materials = [];
                                                        var quantity = [];
                                                        for(var i = 0; i < items.length; i++){
                                                            
                                                            materials[i] = items[i].material;
                                                            quantity[i] = items[i].quantity;
                                                        }
            
                                                        Material.find({_id: materials}, (err, materials)=>{
                                                            if(err){
                                                                res.status(500).send({message:apiMsg});
                                                            }else{
                                                                if(!materials){
                                                                    res.status(404).send({message:"Error al ordenar."});
                                                                }else{
                                                                    if(checkStock(materials, quantity) === false){
                                                                        updateMaterialStock(materials, quantity).then((successMessage) => {
                                                                            if(successMessage){
                                                                                order.items = practice.items;
                                                                                order.save((err, nOrder)=>{
                                                                                    if(err){
                                                                                        res.status(500).send({message:apiMsg});
                                                                                    }else{
                                                                                        if(!nOrder){
                                                                                            res.status(404).send({message:"Error al ordenar."});
                                                                                        }else{

                                                                                            // RETRIEVE POPULATED

                                                                                            var find = Order.findById(nOrder._id);
                                                                                            find
                                                                                            .populate({
                                                                                                path: 'items',
                                                                                                populate: {
                                                                                                    path: 'material',
                                                                                                    model: 'Material'
                                                                                                }
                                                                                            })
                                                                                            .exec((err, order)=>{
                                                                                                if(err){
                                                                                                    res.status(500).send({message:apiMsg});
                                                                                                }else{
                                                                                                    if(!order){
                                                                                                        res.status(404).send({message:"No se encontraron órdenes."});
                                                                                                    }else{
                                                                                                        res.status(200).send(order);
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                           
                                                                                        }
                                                                                    }
                                                                                });
                                                                                //res.status(200).send({message:"Transaccion completada."});
                                                                            }else{
                                                                                res.status(404).send({message:"Error al ordenar."});
                                                                            }
                                                                          });
                                                                    }else{
                                                                        res.status(404).send({message:"No hay material suficiente para procesar la orden."});
                                                                    }
                                                                }
                                                            }
                                                        });   
                                                    }
                                                }
                                            });
            
                                            /*
                                            Material.updateMany({_id: items.material._id, stock: {$gt: 0}}, {$inc: {stock: -1}}, (err, matUp)=>{
                                                if(err){
                                                    res.status(500).send({message:apiMsg});
                                                }else{
                                                    if(!matUp){
                                                        res.status(404).send({message:"Error al ordenar."});
                                                    }else{  
                                                        res.status(200).send(matUp);
                                                    }
                                                }
                                            });
                                            
                                            */
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }else{
        res.status(400).send({message:"Campos insuficientes."});
    }
}

function checkStock(materials, quantity){
    
    var flag = false;
    for(var o = 0; o < quantity.length; o++){
        if( materials[o].stock <  quantity[o] && flag === false ){
            flag = true;
            //res.status(404).send({message:"No hay material suficiente para ordenar."});
        }
    }

    if(flag===true){
        return true;
    }else{
        return false;
    }
}

function updateMaterialStock(materials, quantity){
    var miPrimeraPromise;

    return miPrimeraPromise = new Promise((resolve, reject) => {
        // Llamamos a resolve(...) cuando lo que estabamos haciendo finaliza con éxito, y reject(...) cuando falla.
        // En este ejemplo, usamos setTimeout(...) para simular código asíncrono. 
        // En la vida real, probablemente uses algo como XHR o una API HTML5.
        for(var i = 0; i < materials.length; i++){
            Material.updateOne({$and: [{_id: materials[i]}, {stock: {$gte: quantity[i]}}] }, {$inc: {stock: -quantity[i]}}, (err, matUp)=>{
                if(err){
                    reject(false);
                }else{
                    if(!matUp){
                        reject(false);
                    }else{  
                        resolve(true);
                    }
                }
            });
        }
      });

}

// Removes order
function removeOrder(req, res){
    var orderId = req.params.id;

    Order.findById(orderId, (err, order)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!order){
                res.status(404).send({message:"Error al borrar."});
            }else{
                if(order.status == "PENDIENTE"){
                    Order.findByIdAndRemove(orderId, (err, dOrder)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(!dOrder){
                                res.status(404).send({message:"Error al borrar."});
                            }else{
                                var x = dOrder.items;
                                // DELETE ON CASCADE
                                // Order=>Ban
                                Item.find({_id: x}, (err, items)=>{
                                    if(err){
                                        res.status(500).send({message:apiMsg});
                                    }else{
                                        if(!items){
                                            res.status(404).send({message:"Error al borrar."});
                                        }else{
                                            var material = [];
                                            var quantity = [];
                            
                                            for(var i = 0; i < items.length; i++){
                                                material[i] = items[i].material;
                                                quantity[i] = items[i].quantity;
                                            }
                            
                                            restoreStock(material, quantity).then((successMessage)=>{
                                                if(successMessage){
                                                    Ban.find({order: dOrder._id}).deleteOne((err, banRemoved)=>{
                                                        if(err){
                                                            res.status(500).send({message:apiMsg});
                                                        }else{
                                                            if(!banRemoved){
                                                                res.status(404).send({message:"Error al borrar."});
                                                            }else{
                                                                res.status(200).send(dOrder);
                                                            }
                                                        }
                                                    });
                                                }else{
                                                    res.status(404).send({message:"Error al cancelar orden."});
                                                }
                                            }).catch((errorMsg)=>{
                                                if(errorMsg){
                                                    res.status(404).send({message:"Error al cancelar orden."});
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }else{
                    res.status(404).send({message:"La orden ya ha sido procesada y no puede ser cancelada."});
                }
            }
        }
    });
}

function restoreStock(material, quantity){
    var myPromise;

    return myPromise = new Promise((resolve, reject)=>{
        for(var i = 0; i < material.length; i++){
            Material.updateOne({_id: material[i]._id}, {$inc: {stock: +quantity[i]}}, (err, matUp)=>{
                if(err){
                    reject(err);
                }else{
                    if(!matUp){
                        reject(false);
                    }else{
                        resolve(true);
                    }
                }
            });
        }
    });
}

// Get all orders populating user, materials and practice objects
function getOrders(req, res){
    var find = Order.find({$and: [{exp: {$gt: moment().unix()}}, {status: {$eq: 'PENDIENTE'}}]});

    find
    .populate({path: 'practice'})
    .populate({
        path: 'items',
        populate: {
            path: 'material',
            model: 'Material'
        }
    })
    .populate({path: 'user'})
    .exec((err, orders)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!orders){
                res.status(404).send({message:"No se encontraron órdenes."});
            }else{
                res.status(200).send(orders);
            }
        }
    });
}

function getOrdersExp(req, res){
    var find = Order.find({$and: [{exp: {$lte: moment().unix()}}, {status: {$ne: 'PENDIENTE'} }]});

    find
    .populate({path: 'practice'})
    .populate({
        path: 'items',
        populate: {
            path: 'material',
            model: 'Material'
        }
    })
    .populate({path: 'user'})
    .exec((err, orders)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!orders){
                res.status(404).send({message:"No se encontraron órdenes."});
            }else{
                res.status(200).send(orders);
            }
        }
    });
}

// Get order populating user, materials and practice objects
function getOrder(req, res){
    var orderId = req.params.id;
    var find = Order.findById(orderId);

    find
    .populate({
        path: 'practice'
    })
    .populate({
        path: 'items',
        populate: {
            path: 'material',
            model: 'Material'
        }
    })
    .exec((err, order)=>{
        if(err){
            res.status(500).send({message:apiMsg});
        }else{
            if(!order){
                res.status(404).send({message:"No se encontraron órdenes."});
            }else{
                res.status(200).send(order);
            }
        }
    });
}

// Get all user's active orders populating practice's object
// requires: user_id
function getUserOr(req, res){
    var find = Order.find({$and: [{user: req.user._id}, {exp: {$gt: moment().unix()}}]});

    if(req.user){
        find
        .populate({
            path: 'practice',
            populate: {
                path: 'course items',
                populate: {
                    path: 'material teacher lab'
                }
            }
        })
        .populate({
            path: 'items',
            populate: {
                path: 'material',
                model: 'Material'
            }
        })
        .exec((err, orders)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!orders){
                    res.status(404).send({message:"No se encontraron órdenes."});
                }else{
                    res.status(200).send(orders);
                }
            }
        });
    }else{
        res.status(400).send({message:"Campos insuficientes."});
    }
}

// Get all user's expired orders
// requires: user_id
function getUserExp(req, res){
    var find = Order.find({$and: [{user: req.user._id}, {exp: {$lte: moment().unix()}}]});

    if(req.user){
        find
        .populate({
            path: 'practice',
            populate: {
                path: 'course items',
                populate: {
                    path: 'material teacher lab'
                }
            }
        })
        .populate({
            path: 'items',
            populate: {
                path: 'material',
                model: 'Material'
            }
        })
        .exec((err, orders)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!orders){
                    res.status(404).send({message:"No se encontraron órdenes."});
                }else{
                    res.status(200).send(orders);
                }
            }
        });
    }else{
        res.status(400).send({message:"Campos insuficientes."});
    }
}

function orderReady(req, res){
    var orderId = req.params.id;
    // This registration token comes from the client FCM SDKs.
    var registrationToken;


    if(orderId){
        Order.findByIdAndUpdate(orderId, {status: "OK"},(err, order)=>{
            if(err){
                res.status(500).send({message:apiMsg});
            }else{
                if(!order){
                    res.status(404).send({message:"No se encontró orden."});
                }else{
                    User.findById(order.user, (err, user)=>{
                        if(err){
                            res.status(500).send({message:apiMsg});
                        }else{
                            if(!user){
                                res.status(404).send({message:"No se encontró usuario."});
                            }else{
                                if(user.deviceId != null){
                                    registrationToken = user.deviceId;
                
                                    var message = {
                                        token:registrationToken,
                                        android: {
                                            ttl: 86400 * 1000, // 1 hour in milliseconds
                                            priority: 'high',
                                            notification:{
                                                title:"¡Tu Orden está lista!",
                                                body:"Tu orden ha sido procesada y ahora puedes recogerla al almacén."
                                            }
                                        },
                                        apns: {
                                            headers: {
                                                'apns-priority': '10'
                                            },
                                            payload: {
                                                aps: {
                                                alert: {
                                                    title:"¡Tu Orden está lista!",
                                                    body:"Tu orden ha sido procesada y ahora puedes recogerla al almacén."
                                                },
                                                badge: 42,
                                                }
                                            }
                                        },
                                        webpush: {
                                            notification: {
                                                title:"¡Tu Orden está lista!",
                                                body:"Tu orden ha sido procesada y ahora puedes recogerla al almacén.",
                                                icon: './misc/img/fcq-black.png'
                                            }
                                        },
                                        data:{
                                            order:orderId
                                        }
                                    };
                                    // Send a message to the device corresponding to the provided
                                    // registration token.
                                    admin.messaging().send(message)
                                    .then((response) => {
                                        // Response is a message ID string.
                                        res.status(200).send(order);
                                        console.log('Successfully sent message:', response);
                                    })
                                    .catch((error) => {
                                        res.status(404).send({message:"Error en el dispositivo."});
                                        console.log('Error sending message:', error);
                                    });
                                    
                                }else{
                                    res.status(200).send(order);
                                }
                            }
                        }
                    });
                }
            }
        });
    }else{
        res.status(400).send({message:"Campos insuficientes."});
    }
}

module.exports = {
    testExp,
    testUpdate,
    createOrder,
    removeOrder,
    getOrders,
    getOrdersExp,
    getOrder,
    getUserOr,
    getUserExp,
    orderReady
};